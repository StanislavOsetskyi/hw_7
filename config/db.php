<?php
$host = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbName = 'blog';

try{
    $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
    //Get Exceptions on query errors
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SET UTF
    $pdo->exec('SET NAMES "utf8"');
}catch(Exception $exception){
    echo "Error connecting to db! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die('Wasn\'t able to connect to db!');
}

