<?php
require_once '../config/db.php';
require_once '../Classes/Entries.php';

if(!empty($_POST['title'])){
    $title = htmlspecialchars($_POST['title']);
    $intro = htmlspecialchars($_POST['intro']);
    $content = htmlspecialchars($_POST['content']);
    $entry = new Entries($title,$intro,$content);
    $entry->store($pdo);
}
header('Location:../main.php');