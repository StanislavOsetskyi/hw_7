<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entry</title>
</head>
<body>
    <h1>Create Entry</h1>
    <form action="/hw_7/entries/storeEntry.php" method="post">
        <label>Entry title: <input type="text" name="title"> </label>
        <br>
        <label>Entry intro: <input type="text" name="intro"> </label>
        <br>
        <label>Entry content: <input type="text" name="content"> </label>
        <br>
        <button>Save Entry</button>
    </form>
</body>
</html>
