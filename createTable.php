<?php
require_once 'config/db.php';

try {
    $entriesSql = "CREATE TABLE entries (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR (255),
    intro VARCHAR (255),
    content TEXT
    )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($entriesSql);

    $commentsSql = "CREATE TABLE comments (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255),
    body TEXT,
    entry_id INT,
    FOREIGN KEY (entry_id) REFERENCES entries (id)
    )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($commentsSql);

}catch (Exception $exception){
    echo 'Error creating table'. $exception->getMessage();
}
header('Location: main.php');