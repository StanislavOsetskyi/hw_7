<?php
require_once 'config/db.php';
require_once 'Classes/Comments.php';
require_once 'Classes/Entries.php';


$comments = Comments::all($pdo);
$entries = Entries::all($pdo);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>
</head>
<body>
<h1>Entries:</h1>
<?php foreach ($entries as $entry):?>
    <div>
        <h2><?=$entry->getTitle()?></h2>

        <?=$entry->getIntro()?>
        <br>
        <?=$entry->getContent()?>
        <br>
        <div>
            <ul>
                <?php foreach ($entry->getComments($pdo) as $comment):?>
                    <li><?=$comment->getName().' say: '.$comment->getBody()?></li>
                <?php endforeach;?>
            </ul>
        </div>
        <form action="/hw_7/comments/createComment.php">
            <button>Comment</button>
        </form>
    </div>
<?php endforeach;?>
    <br>
    <form action="/hw_7/entries/createEntry.php">
        <button>Create new entry</button>
    </form>

</body>
</html>

