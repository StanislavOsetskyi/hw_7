<?php


class Entries
{
    protected $id = 0;
    protected $title = 0;
    protected $intro = 0;
    protected $content = 0;


    public function __construct($title,$intro,$content)
    {
        $this->title = $title;
        $this->intro = $intro;
        $this->content = $content;
    }

    public function  store($pdo){
        try {
            $sql = "INSERT INTO entries SET 
            title = :title,
            intro = :intro,
            content = :content";

            $statement = $pdo->prepare($sql);
            $statement -> bindValue(':title', $this->getTitle());
            $statement -> bindValue(':intro', $this->getIntro());
            $statement -> bindValue(':content', $this->getContent());
            $statement->execute();
        }catch(Exception $exception){
            die('что-то пошло не так'.$exception->getMessage());
        }

    }

    static public function all(PDO $pdo){
        try{
            $sql = "SELECT * FROM entries";
            $statement = $pdo->query($sql);
            $entriesArray = $statement->fetchAll();
            $entryObj = [];
            foreach ($entriesArray as $entriesArr){
                $entry = new self($entriesArr['title'],$entriesArr['intro'],$entriesArr['content']);
                $entry -> setId($entriesArr['id']);
                $entryObj[] = $entry;

            }
            return $entryObj;

        }catch (Exception $exception){
            die('Что-то пошло не так');
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getComments(PDO $pdo){
        $sql = "SELECT * FROM comments WHERE entry_id =".$this->id;
        $statement = $pdo->query($sql);
        $commentsArr = $statement->fetchAll();
        $commentsObjs = [];
        foreach ($commentsArr as $comment){
            $commentObj = new Comments($comment['name'],$comment['body'],$comment['entry_id']);
            $commentObj ->setId($comment['id']);
            $commentsObjs[] = $commentObj;
        }
        return $commentsObjs;
    }


}