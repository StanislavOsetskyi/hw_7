<?php


class Comments
{
    protected $id;
    protected $name;
    protected $body;
    protected $entry_id;
    public function __construct($name, $body,$entry_id = NULL)
    {
        $this->name = htmlspecialchars($name);
        $this->body = htmlspecialchars($body);
        $this->entry_id = htmlspecialchars($entry_id);
    }

    public function  store($pdo){
        try {
            $sql = "INSERT INTO comments SET 
            name = :name,
            body = :body,
            entry_id = :entry_id ";
            $statement = $pdo->prepare($sql);
            $statement -> bindValue(':name', $this->getName());
            $statement -> bindValue(':body', $this->getBody());
            $statement->bindValue('entry_id', $this->getEntryId()) ;
            $statement->execute();
        }catch(Exception $exception){
            die('что-то пошло не так'.$exception->getMessage());
        }

    }
    public function getEntryId()
    {
        return $this->entry_id;
    }
    static public function all(PDO $pdo){
        try{
            $sql = "SELECT * FROM comments";
            $statement = $pdo->query($sql);
            $commentsArray = $statement->fetchAll();
            $commentObj = [];
            foreach ($commentsArray as $commentsArr){
                $comment= new self($commentsArr['name'],$commentsArr['body']);
                $comment -> setId($commentsArr['id']);
                $commentObj[] = $comment;

            }
            return $commentObj;

        }catch (Exception $exception){
            die('Что-то пошло не так');
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


}

