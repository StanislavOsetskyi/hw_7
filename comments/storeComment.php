<?php
require_once '../config/db.php';
require_once '../Classes/Comments.php';
if(!empty($_POST['name'])){
    $name = htmlspecialchars($_POST['name']);
    $body = htmlspecialchars($_POST['body']);
    $entryId = htmlspecialchars($_POST['entry_id']);
    $comment = new Comments($name,$body,$entryId);
    $comment->store($pdo);
}
header('Location:../main.php');
