<?php
require_once '../config/db.php';
require_once '../Classes/Entries.php';
$entries = Entries::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comment</title>
</head>
<body>
    <h1>Create Comment</h1>
    <form action="/hw_7/comments/storeComment.php" method="post">
        <label> Name: <input type="text" name="name"> </label>
        <br>
        <label>Comment: <input type="text" name="body"> </label>
        <br>
        <div>
            <label for="entry">Select entry</label>
            <select name="entry_id" id="entry">
                <?php foreach ($entries as $entry): ?>
                <option value="<?=$entry->getId()?>"><?=$entry->getTitle()?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <button>Save Comment</button>
    </form>
</body>
</html>

